<?php
/**
 *
 * @package agata
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$agata_includes = array(
	'/theme-settings.php',                  
	'/setup.php',                           
	'/widgets.php',                         
	'/enqueue.php',                         
	'/template-tags.php',                   
	'/pagination.php',                      
	'/hooks.php',                           
	'/extras.php',                          
	'/customizer.php',                      
	'/custom-comments.php',                 
	'/jetpack.php',                         
	'/class-wp-bootstrap-navwalker.php',    
	'/editor.php',                          
);

foreach ( $agata_includes as $file ) {
	$filepath = locate_template( '/inc' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}

function properties_post_type() {
	$labels = array(
	  'name'               => _x( 'Propiedades', 'post type general name' ),
	  'singular_name'      => _x( 'Propiedad', 'post type singular name' ),
	  'add_new'            => _x( 'Agregar propiedad', 'agata' ),
	  'add_new_item'       => __( 'Agregar nueva propiedad' ),
	  'edit_item'          => __( 'Editar propiedad' ),
	  'new_item'           => __( 'Nueva propiedad' ),
	  'all_items'          => __( 'Todas las propiedades' ),
	  'view_item'          => __( 'Ver propiedades' ),
	  'search_items'       => __( 'Buscar propiedades' ),
	  'not_found'          => __( 'No se encontró propiedades' ),
	  'not_found_in_trash' => __( 'No se encontraron propiedades' ), 
	  'parent_item_colon'  => '',
	  'menu_name'          => 'Propiedades'
  );
  
  $args = array(
	'labels'                => $labels,
	'description'           => 'Propiedades',
	'public'                => true,
	'publicaly_queryable'   => true,
	'show_ui'               => true,
	'show_in_menu'          => true,
	'query_var'             => true,
	'rewrite'               => array( 'slug' => 'propiedad' ),
	'capability_type'       => 'post',
	'has_archive'           => true,
	'Hierarchical'          => false,
	'menu_position'         => null,
	'menu_icon'             => 'dashicons-admin-home',
	'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','hierarchical','trackbacks','custom-fields','revisions','page-attributes'),
	); 
  
  register_post_type( 'propiedad', $args );   
  }
  add_action( 'init', 'properties_post_type' );


  // Crear taxonomías
  add_action( 'init', 'create_properties_features', 0 );
  function create_properties_features() {
		$labels = array(
			'name' => _x( 'Características', 'agata' ),
			'singular_name' => _x( 'Característica', 'agata' ),
			'search_items' =>  __( 'Buscar Característica' ),
			'all_items' => __( 'Todas las Características' ),
			'parent_item' => __( 'Característica padre' ),
			'parent_item_colon' => __( 'Característica padre:' ),
			'edit_item' => __( 'Editar Característica' ),
			'update_item' => __( 'Actualizar Característica' ),
			'add_new_item' => __( 'Añadir nueva Característica' ),
			'new_item_name' => __( 'Nombre de la nueva Característica' ),
		);
		register_taxonomy( 'caracteristica', array( 'propiedad' ), array(
			'hierarchical' => true,
			'labels' => $labels, 
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'caracteristica' ),
		));
	}

	add_action( 'init', 'create_properties_types', 0 );
  	function create_properties_types() {
		$labels = array(
			'name' => _x( 'Tipos', 'agata' ),
			'singular_name' => _x( 'Tipo', 'agata' ),
			'search_items' =>  __( 'Buscar Tipo' ),
			'all_items' => __( 'Todos los Tipos' ),
			'parent_item' => __( 'Tipo padre' ),
			'parent_item_colon' => __( 'Tipo padre:' ),
			'edit_item' => __( 'Editar Tipo' ),
			'update_item' => __( 'Actualizar Tipo' ),
			'add_new_item' => __( 'Añadir nuevo Tipo' ),
			'new_item_name' => __( 'Nombre del nuevo Tipo' ),
		);
		register_taxonomy( 'tipo', array( 'propiedad' ), array(
			'hierarchical' => true,
			'labels' => $labels, 
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'tipo' ),
		));
	}



	function agata_login_logo() { ?>
		<style type="text/css">
			#login h1 a, .login h1 a {
				background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/logo_98x98@2x.png);
				height:98px;
				width:98px;
				background-size: 98px 98px;
				background-repeat: no-repeat;
				padding-bottom: 10px;
			}
		</style>
	<?php }
	add_action( 'login_enqueue_scripts', 'agata_login_logo' );

	
	remove_action( 'load-update-core.php', 'wp_update_plugins' );
	add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );

	
	// Marcar menú propiedades cuando se esté en la taxonomía "Tipo"
	function my_special_nav_class( $classes, $item ) {
		if ( is_tax() && $item->title == 'Propiedades' ) {
			$classes[] = 'current-menu-item';
		}
		return $classes;
	}    
	add_filter( 'nav_menu_css_class', 'my_special_nav_class', 10, 2 );


	add_action('pre_get_posts', function($query)
	{
		if ($query->is_tax('tipo')) {
			$query->set('posts_per_page', 6);
		}
	});