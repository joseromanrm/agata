<?php
/**
 *
 * @package agata
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<footer>
	<div class="pre-footer jsWaypoint--appearReveal">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-5 align-self-center">
					<?php 
					$copyright   = get_theme_mod( 'nic_theme_copyright' );
					if( empty(trim($copyright)) )
						echo "© 2018 Agata Bienes Raíces";
					else	
						echo $copyright;
				?>
				</div>

				<div class="col-lg-6 col-md-7 align-self-center right">
					<?php get_template_part( 'global-templates/links-hf' ); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="menu-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 right">
					<?php 
						wp_nav_menu( array( 'theme_location' => 'footer' ) ); 
					?>

				</div>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/shortcuts/inview.js"></script>


</body>

</html>
