<?php
/**
 * @package agata
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container   = get_theme_mod( 'nic_container_type' );

?>

<h1 class="page-title"><?php printf(
							 esc_html__( 'Search Results for: %s', 'agata' ),
								'<span>' . get_search_query() . '</span>' ); ?></h1>
<main class="site-main" id="main">

<?php if ( have_posts() ) : ?>


	<?php while ( have_posts() ) : the_post(); ?>

		<?php
		get_template_part( 'loop-templates/content', 'search' );
		?>

	<?php endwhile; ?>

<?php else : ?>

	<?php get_template_part( 'loop-templates/content', 'none' ); ?>

<?php endif; ?>

</main><!-- #main -->

<!-- The pagination component -->
<?php nic_pagination(); ?>

<?php get_footer(); ?>
