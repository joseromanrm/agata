<?php
/**
 * @package agata
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

?>

<?php get_template_part( 'global-templates/categories' ); ?>
<?php get_template_part( 'global-templates/filter' ); ?>

<div class="all-properties">
	<div class="container">

		<div class="row">

			<?php
                $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
				$paged= (get_query_var('paged' )) ? get_query_var('paged'):1; 
				
				//var_dump($term);
				//echo $term->slug;
				if(empty($term->slug)){
					
					$query = new WP_Query(array(
						'post_type' => 'propiedad',
						'posts_per_page' => 6,
						'post_status' => 'publish',
						'paged' => $paged,
						
						
					));
				}else{	
					$query = new WP_Query(array(
						'post_type' => 'propiedad',
						'posts_per_page' => 6,
						'post_status' => 'publish',
						'paged' => $paged,
						'tax_query' => array(
							array(
								'taxonomy' => 'tipo',
								'field' => 'slug',
								'terms' => $term->slug,
							),
						),
						
					));
				}
				
				while ($query->have_posts()) {
					$query->the_post();
					$post_id = get_the_ID();
					
					$province = trim(get_field('propiedad_ubicacion', $post_id));
					$location = trim(get_field('propiedad_ubicacion_2', $post_id));
					$price = trim(get_field('propiedad_precio', $post_id));
                    $url = get_permalink();
                    
                    $thumb_id = get_post_thumbnail_id();
                    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'property-thumbnail-size', true);
					$imagen = $thumb_url_array[0];
                    
                    //$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
					//echo $term->slug;
					
					$moneda = get_field('tipo_de_moneda', $post_id );
					if( empty($moneda) ){
						$currency   = get_theme_mod( 'nic_theme_currency' );
						if( empty(trim($currency)) ) $currency = "₡";
					}else{
						$currency = $moneda;
					}
				
			?>
			<div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="100" data-wow-delay="0s">
			<div class="properties" data-url="<?php echo $url; ?>">
					<div class="property">
						<div class="hover"><a href="#">Info</a></div>
						<div class="location"><?php echo $location . ', ' . $province; ?></div>
						<?php 
							if(!empty($price)){
								echo '<div class="price">'.$currency.number_format($price).'</div>';
							}
						?>
						<img class="img-fluid" src="<?php echo $imagen; ?>" alt="">
					</div>
					<div class="title"><?php echo get_the_title(); ?></div>
				</div>
			</div>
			<?php
				}
				
				wp_reset_query();
            ?>
            
            <div class="col-lg-12">
				<div class="pager">
					<?php
					
						echo paginate_links( array(
							'base' => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
							'format' => '?paged=%#%',
							'current' => max( 1, get_query_var('paged') ),
							'total' => $query->max_num_pages
					   ) );
					?>
				</div>
			</div>
		
		</div>
	</div>
</div>

<?php get_template_part( 'global-templates/categories' ); ?>

<?php get_footer(); ?>
