<?php
/**
 *
 * @package agata
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<section class="no-results not-found">

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<img src="<?php echo get_template_directory_uri(); ?>/img/home.svg" alt="">
				<h2 class="page-title">
					<?php esc_html_e( 'No se encontraron resultados', 'agata' ); ?>
				</h2>
			</div>
		</div>
	</div>

</section>
