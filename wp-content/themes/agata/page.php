<?php
/**
 * @package agata
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container   = get_theme_mod( 'nic_container_type' );

?>

<section class="pages">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php
						the_content();
						?>

					<?php endwhile; ?>
				<?php else : ?>

					<?php get_template_part( 'loop-templates/content', 'none' ); ?>

				<?php endif; ?>
		</div>
	</div>
</div>
</section>

<?php get_footer(); ?>
