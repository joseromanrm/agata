<div class="filter wow fadeInUp" data-wow-duration="100" data-wow-delay="0s">
<form method="post" id="searchform" action="<?php echo get_home_url(); ?>/propiedades/" role="search">
    <div class="container">
        <div class="row">
        
            <div class="col-lg-1 col-md-12 align-self-center right d-none d-lg-block">
                <svg width="33px" height="33px" viewBox="0 0 33 33" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                        <polygon id="path-1" points="0.0004 -0.0001 32.129 -0.0001 32.129 32.1195 0.0004 32.1195"></polygon>
                    </defs>
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="v" transform="translate(-324.000000, -223.000000)">
                            <g id="Page-1" transform="translate(324.000000, 223.000000)">
                                <mask id="mask-2" fill="white">
                                    <use xlink:href="#path-1"></use>
                                </mask>
                                <g id="Clip-2"></g>
                                <path d="M13.2584,5.2289 C17.6654,5.2289 21.2884,8.8139 21.2884,13.2589 C21.2884,17.6659 17.6654,21.2889 13.2584,21.2889 C8.8144,21.2889 5.2284,17.6659 5.2284,13.2589 C5.2284,8.8139 8.8144,5.2289 13.2584,5.2289 M13.2584,26.5179 C15.9854,26.5179 18.4874,25.6959 20.5784,24.3139 L27.6374,31.3349 C28.1604,31.8579 28.8324,32.1199 29.5054,32.1199 C30.1774,32.1199 30.8494,31.8579 31.3724,31.3349 C32.3814,30.3269 32.3814,28.6459 31.3724,27.6379 L24.2764,20.5789 C25.6954,18.4869 26.4804,15.9479 26.4804,13.2589 C26.5174,5.9379 20.5784,-0.0001 13.2584,-0.0001 C5.9384,-0.0001 0.0004,5.9379 0.0004,13.2589 C0.0004,20.5789 5.9384,26.5179 13.2584,26.5179" id="Fill-1" fill="#FFFFFF" mask="url(#mask-2)"></path>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>

            

            <div class="col-lg-3 col-md-2 col-5 align-self-center right xs-mb-20">
                <span>Tipo de propiedad</span>
            </div>

            <div class="col-lg-2 col-md-3 col-7 align-self-center xs-mb-20">
                    <?php
                        $args = array (
                            'type' => 'propiedad', //your custom post type
                            'orderby' => 'name',
                            'order' => 'ASC',
                            'taxonomy' => 'tipo',
                            'hide_empty' => 0 //shows empty categories
                        );
                        $categories = get_categories( $args );
                        if( !empty($categories) ){
                    ?>
                    <div class="box-select">
                    <select name="tipo" id="tipo">
                            <option selected value="">Todos</option>
                            <?php 
                                foreach ($categories as $category) {   
                                    echo '<option value="'.$category->slug.'">'.$category->name.'</option>'; 
                                }
                            ?>
                    </select>
                            </div>
                    <?php } ?>
            </div>

            <div class="col-lg-2 col-md-2 col-5 align-self-center right xs-mb-20">
                <span>Ubicación</span>
            </div>

            <div class="col-lg-2 col-md-3 col-7 align-self-center xs-mb-20">
                    <div class="box-select">
											<select name="lugar" id="lugar">
												<option value="">Todos</option>
												<option value="san jose">San José</option>
												<option value="alajuela">Alajuela</option>
												<option value="cartago">Cartago</option>
												<option value="heredia">Heredia</option>
												<option value="guanacaste">Guanacaste</option>
												<option value="puntarenas">Puntarenas</option>
												<option value="limón">Limón</option>
											</select>
											</div>
            </div>

            <!--div class="col-lg-2 col-md-2 col-5 align-self-center right xs-mb-20">
               <span>Rango de precio</span>
            </div>

            <div class="col-lg-2 col-md-3 col-7 align-self-center xs-mb-20">
            <div class="box-select">
                        
                        <select name="rango_precio" id="rango_precio">
                            <option selected value="all">Todos</option>
                            <option value="1-50000">$0 - $50,000</option>
                            <option value="50001-100000">$50,000 - $100,000</option>
                            <option value="100001-200000">$100,000 - $200,000</option>
                            <option value="200001-300000">$200,000 - $300,000</option>
                            <option value="300001-400000">$300,000 - $400,000</option>
                            <option value="400001-500000">$400,000 - $500,000</option>
                        </select>
                        </div> 
            </div-->

            <div class="col-lg-2 col-md-2 align-self-center">
                <button type="submit" class="btn btn-lg btn-block">Buscar</button>
            </div>
        </div>
    </div>
    </form>
</div>