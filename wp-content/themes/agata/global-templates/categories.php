<?php
    $args = array (
        'type' => 'propiedad', //your custom post type
        'orderby' => 'name',
        'order' => 'ASC',
        'taxonomy' => 'tipo',
        'hide_empty' => 0 //shows empty categories
    );
    $categories = get_categories( $args );

    $classActive = '';

    if( !empty($categories) ){

        $current_tax = get_query_var( 'tipo' );
        $term =get_term_by( 'slug', $current_tax, 'tipo');
        $term_slug = $term->slug; 
?>
<div class="properties-categories wow fadeInUp" data-wow-duration="100" data-wow-delay="0s">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">
                <ul>
                    <li>
                            <a class="<?php if( $post->ID == 11  ) echo 'active'; ?>" href="<?php echo get_home_url() . '/propiedades/'; ?>">Todas las propiedades</a> 
                    </li><?php 

                    foreach ($categories as $category) { 
                        //echo $term_slug . '-' . $category->slug . ' +++++++++++++ ';
                            if( ($term_slug == $category->slug) && is_tax() ) $classActive = 'active'; else $classActive='';
                            echo '<li><a class="'.$classActive.'" href="'.get_term_link($category->slug, 'tipo').'">'.$category->name.'</a></li>';
                    }


                ?>
                </ul>
                
            </div>

            <div class="col-md-12 center d-block d-lg-none align-self-center">
                <h4>Tipo de propiedad</h4>
                <select name="tipo_propiedad" id="tipo_propiedad">
                    <option value="<?php echo get_home_url() . '/tipo/'; ?>">Todas las propiedades</option>
                    <?php
                        foreach ($categories as $category) {   
                            echo '<option value="'.get_term_link($category->slug, 'tipo').'">'.$category->name.'</option>';
                        }
                    ?>
				</select>
            </div>
        </div>
    </div>
</div>

<?php } ?>