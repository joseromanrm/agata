<div class="links-properties wow fadeInUp" data-wow-duration="100" data-wow-delay="0s">
			<div class="container">
				<div class="row">

                    <div class="col align-self-end wow fadeInUp" data-wow-duration="100" data-wow-delay="0.2s">
						<a href="<?php echo get_home_url(); ?>/tipo/fincas/">
							<img src="<?php echo get_template_directory_uri(); ?>/img/home/fincas.svg" alt="">
							<h4>Fincas</h4>
						</a>
					</div>

                    <div class="col align-self-end wow fadeInUp" data-wow-duration="100" data-wow-delay="0.3s">
						<a href="<?php echo get_home_url(); ?>/tipo/quintas/">
							<img src="<?php echo get_template_directory_uri(); ?>/img/home/quintas.svg" alt="">
							<h4>Quintas</h4>
						</a>
					</div>

                    <div class="col align-self-end wow fadeInUp" data-wow-duration="100" data-wow-delay="0.1s">
						<a href="<?php echo get_home_url(); ?>/tipo/lotes/">
							<img src="<?php echo get_template_directory_uri(); ?>/img/home/lotes.svg" alt="">
							<h4>Lotes</h4>
						</a>
					</div>


					<div class="col align-self-end wow fadeInUp" data-wow-duration="100" data-wow-delay="0s">
						<a href="<?php echo get_home_url(); ?>/tipo/casas/">
							<img src="<?php echo get_template_directory_uri(); ?>/img/home/casas.svg" alt="">
							<h4>Casas</h4>
						</a>
					</div>
					
					<div class="col align-self-end wow fadeInUp" data-wow-duration="100" data-wow-delay="0.4s">
						<a href="<?php echo get_home_url(); ?>/tipo/comercios/">
							<img src="<?php echo get_template_directory_uri(); ?>/img/home/comercios.svg" alt="">
							<h4>Comercios</h4>
						</a>
					</div>
				</div>
                
			</div>
</div>