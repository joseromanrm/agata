
<div class="recents">
	<div class="container">

		<div class="row">
			<div class="col-lg-12 wow fadeInUp" data-wow-duration="100" data-wow-delay="0s">
				<h3>Propiedades similares</h3>
			</div>
			<?php
				$query = new WP_Query(array(
					'post_type' => 'propiedad',
					'posts_per_page' => 6,
                    'post_status' => 'publish',
                    'orderby' => 'random'
				));
				
				while ($query->have_posts()) {
					$query->the_post();
					$post_id = get_the_ID();

					$province = trim(get_field('propiedad_ubicacion', $post_id));
					$location = trim(get_field('propiedad_ubicacion_2', $post_id));
					$price = trim(get_field('propiedad_precio', $post_id));
					$url = get_permalink();

					$thumb_id = get_post_thumbnail_id();
                    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'property-thumbnail-size', true);
					$imagen = $thumb_url_array[0];
					
					$moneda = get_field('tipo_de_moneda', $post_id );
					if( empty($moneda) ){
						$currency   = get_theme_mod( 'nic_theme_currency' );
						if( empty(trim($currency)) ) $currency = "₡";
					}else{
						$currency = $moneda;
					}
				
			?>
			<div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="100" data-wow-delay="0s">
				<div class="properties" data-url="<?php echo $url; ?>">
					<div class="property">
						<div class="hover"><a href="#">Info</a></div>
						<div class="location"><?php echo $location . ', ' . $province; ?></div>
						<?php 
							if(!empty($price)){
								echo '<div class="price">'.$currency.number_format($price).'</div>';
							}
						?>
						<img class="img-fluid" src="<?php echo $imagen; ?>" alt="">
					</div>
					<div class="title"><?php echo get_the_title(); ?></div>
				</div>
			</div>
			<?php
				}
				
				wp_reset_query();
			?>
		
		</div>
	</div>
</div>