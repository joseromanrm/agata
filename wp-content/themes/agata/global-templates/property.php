<?php
$currency   = get_theme_mod( 'nic_theme_currency' );
if( empty(trim($currency)) ) $currency = "₡";
?>
			<div class="col-lg-4 col-md-6 col-12">
				<div class="properties" data-url="<?php echo $url; ?>">
					<div class="property">
						<div class="hover"><a href="#">Info</a></div>
						<div class="location"><?php echo $location . ', ' . $province; ?></div>
						<?php 
							if(!empty($price)){
								echo '<div class="price">'.$currency.$price.'</div>';
							}
						?>
						<div class="price"><?php echo $currency; ?><?php echo $price; ?></div>
						<img class="img-fluid" src="<?php echo $imagen; ?>" alt="">
					</div>
					<div class="title"><?php echo get_the_title(); ?></div>
				</div>
			</div>
	