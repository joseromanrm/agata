<?php /* Template Name: Propiedad */ ?>

<?php get_header(); ?>

<div class="actions">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-12 align-self-center xs-center">
				<a href="#">Volver</a>
			</div>
			<div class="col-lg-6 col-md-6 col-12 xs-center xs-mt-30 right align-self-center">
				<button type="button" class="btn"><svg width="34px" height="34px" viewBox="0 0 34 34" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="v" transform="translate(-408.000000, -349.000000)" fill="#187F2E">
            <path d="M435.355,370.0428 C433.065,370.0428 431.083,371.2718 429.974,373.0918 L420.6,370.2808 C420.604,370.2008 420.625,370.1248 420.625,370.0428 C420.625,369.0238 420.362,368.0728 419.933,367.2228 L427.587,360.5248 C428.602,361.2178 429.825,361.6258 431.146,361.6258 C434.633,361.6258 437.459,358.7998 437.459,355.3138 C437.459,351.8268 434.633,349.0008 431.146,349.0008 C427.66,349.0008 424.834,351.8268 424.834,355.3138 C424.834,356.3318 425.096,357.2808 425.526,358.1328 L417.871,364.8308 C416.856,364.1388 415.633,363.7308 414.312,363.7308 C410.826,363.7308 408,366.5558 408,370.0428 C408,373.5298 410.826,376.3558 414.312,376.3558 C416.599,376.3558 418.585,375.1268 419.693,373.3068 L429.067,376.1178 C429.063,376.1998 429.042,376.2728 429.042,376.3558 C429.042,379.8418 431.868,382.6678 435.355,382.6678 C438.841,382.6678 441.667,379.8418 441.667,376.3558 C441.667,372.8688 438.841,370.0428 435.355,370.0428" id="Page-1"></path>
        </g>
    </g>
</svg> Compartir</button>
			</div>
		</div>
	</div>
</div>

<div class="description">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 mb-50">
				<h4>Ubicación: Ciudad Colón, San José</h4>
			</div>

			<div class="col-lg-8 col-md-6 xs-mb-30">
				<div class="gallery">
					<a href="#" class="carousel-left"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/carousel-left.svg" alt=""></a>
					<a href="#" class="carousel-right"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/carousel-right.svg" alt=""></a>
					<div id="sync1" class="owl-carousel owl-theme">
						<div class="item">
							<img src="<?php echo get_template_directory_uri(); ?>/img/properties/gallery.jpg" alt="">
						</div>
						<div class="item">
							<img src="<?php echo get_template_directory_uri(); ?>/img/properties/gallery.jpg" alt="">
						</div>

						<div class="item">
							<img src="<?php echo get_template_directory_uri(); ?>/img/properties/gallery.jpg" alt="">
						</div>
					</div>
				</div>

				<div id="sync2" class="owl-carousel owl-theme">
					<div class="item">
						<img src="<?php echo get_template_directory_uri(); ?>/img/properties/gallery.jpg" alt="">
					</div>

					<div class="item">
						<img src="<?php echo get_template_directory_uri(); ?>/img/properties/gallery.jpg" alt="">
					</div>

					<div class="item">
						<img src="<?php echo get_template_directory_uri(); ?>/img/properties/gallery.jpg" alt="">
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<p>DESCRIPCIÓN:</p>
				<p>Casa recién renovada con finos acabados que ofrece una excelente ubicación en Ciudad Colón cerca de comercios, paradas de bus, supermercados.</p>
				<p>Consta de 3 dormitorios, 2 baños, 2 plantas, rodeada de jardines, con estufa, horno y lavadora de platos, cuarto de de pilas,  tiene agua caliente en toda la propiedad.</p>

				<div class="info-description">
					<h5>Contáctenos</h5>
					<a class="mail" href="#">info@agatabienesraices.com</a>
					<?php get_template_part( 'global-templates/links-hf' ); ?>
				</div>
			</div>
		</div>
	</div>
</div>


<?php
				$args = array (
					'type' => 'propiedad', //your custom post type
					'orderby' => 'name',
					'order' => 'ASC',
					'taxonomy' => 'caracteristica',
					'hide_empty' => 0 //shows empty categories
				);
				$caracteristicas = get_categories( $args );
			
				if( !empty($caracteristicas) ){
					
				
?>

<div class="features">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3>Características</h3>
			</div>

			<?php
				foreach ($caracteristicas as $caracteristica) {        
					//echo '<li><a href="'.get_term_link($caracteristica->slug, 'tipo').'">'.$caracteristica->name.'</a></li>';
			?>
				<div class="col-lg-4 col-md-6 col-12 col-border mb-0">
					<ul><li><i class="icon-caracteristica_propiedad"></i> <?php echo $caracteristica->name; ?></li></ul>
				</div>
			<?php } ?>

			<!--div class="col-lg-4 col-md-6 col-12 col-border mb-0">
				<ul>
					<li><i class="icon-caracteristica_propiedad"></i> Tamaño de la propiedad: 400 m</li>
					<li><i class="icon-casas"></i> Tamaño de construcción: 100 m</li>
					<li><i class="icon-caracteristica_habitaciones"></i> 2 habitaciones</li>
					<li><i class="icon-caracteristica_banos"></i> 2.5 baños</li>
					
				</ul>
			</div>

			<div class="col-lg-4 col-md-6 col-12 col-border mb-0">
				<ul>
					<li><i class="icon-caracteristica_garage"></i> Garaje para 2 carros</li>
					<li><i class="icon-caracteristica_cocina"></i> Cocina con desayunador</li>
					<li><i class="icon-caracteristica_salaTV"></i> Sala de TV</li>
					<li class="none"><i class="icon-none"></i> Amplia Sala</li>
		
				</ul>
			</div>

			<div class="col-lg-4 col-md-6 col-12 col-border mb-0">
				<ul>
					<li><i class="icon-caracteristica_comedor"></i> Amplio comedor</li>
					<li><i class="icon-caracteristica_lavado"></i> Zona de lavado</li>
			
				</ul>
			</div-->
			
		</div>
	</div>
</div>

<?php } ?>

<?php get_template_part( 'global-templates/similar' ); ?>

<?php get_footer(); ?>

