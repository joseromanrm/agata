(function ($) {
    $(function () {

        $(window).on('load', function () {
            $('.loader').fadeOut();
        });

        // Función para hacer transition lenta en el cambio de página
        function change_page(goTo) {
            $('html').css('background', '#187f2e');
            $('body').fadeOut('slow', function () {
                window.location.href = goTo;
            });
        }

        $(document).ready(function () {

            $('.jsWaypoint--appearReveal').each(function(){
                var shown = $(this);
                new Waypoint.Inview({
                      element: this,
                      //element: $('section[class^="jsWaypoint--whiteSkinOn"]'),
                      offset: '0%',
                      enter: function(direction) {
                        //console.log('Enter triggered with direction ' + direction);
                      },
                      entered: function(direction) {
                        $('#back-to-top').addClass('is--shown');
                        //console.log('Entered triggered with direction ' + direction);
                      },
                      exit: function(direction) {
                        //console.log('Exit triggered with direction ' + direction);
                      },
                      exited: function(direction) {
                        $('#back-to-top').removeClass('is--shown');
                        //console.log('Exited triggered with direction ' + direction);
                      }
                })
            })

            $('body').on('click', '.properties', function (e) {
                e.preventDefault();
                var property_url = $(this).attr('data-url');
                //window.location.href = property_url;
                change_page(property_url);
            });

            $('body').on('click', '.see-all-properties, header .menu ul li a, header .logo a, .menu-footer ul li a, .properties-categories ul li a, a.volver', function (e) {
                e.preventDefault();
                var link = $(this).attr('href');
                if (link != '') change_page(link);
            });

            $('body').on('mouseover', '.links-properties a', function (e) {
                e.preventDefault();
                var obj = $(this);
                $(".links-properties a").not(this).addClass("opacity");
            });

            $('body').on('mouseleave', '.links-properties a', function (e) {
                e.preventDefault();
                $('.links-properties a').removeClass('opacity');
            });

            $("select[name='tipo_propiedad']").change(function (e) {
                window.location.href = $(this).find(":checked").val();
            });

            $("header .apply-sticky").sticky({
                topSpacing: 0,
                zIndex: 99999
            });

            $('.menu-responsive button').on('click', function () {
                if ($(this).hasClass('is-active')) {
                    $(this).removeClass('is-active');
                    $('.menu-mobile').slideUp();
                } else {
                    $(this).addClass('is-active');
                    $('.menu-mobile').slideDown();
                }
            })

            var scrollTrigger = 200,
                backToTop = function () {
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > scrollTrigger) {
                        $('#back-to-top').addClass('show');
                    } else {
                        $('#back-to-top').removeClass('show');
                    }
                };
            backToTop();
            $(window).on('scroll', function () {
                backToTop();
            });
            $('#back-to-top').on('click', function (e) {
                e.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
            });

            var sync1 = $('#sync1'),
                sync2 = $('#sync2'),
                duration = 300,
                thumbs = 7;


            $('body').on('click', '.carousel-right', function (e) {
                sync1.trigger('next.owl.carousel');
                sync2.trigger('next.owl.carousel');
                e.preventDefault();

            });
            $('body').on('click', '.carousel-left', function (e) {
                sync1.trigger('prev.owl.carousel');
                sync2.trigger('prev.owl.carousel');
                e.preventDefault();

            });

            sync1.on('click', '.owl-next', function () {
                sync2.trigger('next.owl.carousel')
            });
            sync1.on('click', '.owl-prev', function () {
                sync2.trigger('prev.owl.carousel')
            });

            sync1.owlCarousel({
                    rtl: false,
                    center: false,
                    loop: true,
                    items: 1,
                    margin: 0,
                    nav: false,
                    dots: false
                })
                .on('dragged.owl.carousel', function (e) {
                    var item = e.item.index;
                    console.log(item);
                    sync2.trigger('to.owl.carousel', item);

                });


            sync2.owlCarousel({
                    rtl: false,
                    center: false,
                    loop: false,
                    items: thumbs,
                    margin: 10,
                    nav: false,
                    dots: false,
                    responsive: {
                        0: {
                            items: 3
                        },
                        600: {
                            items: 5
                        },
                        1000: {
                            items: 7
                        }
                    }
                })
                .on('click', '.owl-item', function (e) {
                    /*var i = $(this).index()-(thumbs);
                    sync2.trigger('to.owl.carousel', [i]);
                    sync1.trigger('to.owl.carousel', [i]);*/
                    e.preventDefault();
                    sync1.trigger('to.owl.carousel', [$(e.target).parents('.owl-item').index(), 300, true]);
                    sync2.trigger('to.owl.carousel', [$(e.target).parents('.owl-item').index(), 300, true]);
                });


        })

    });
})(jQuery)


var wow = new WOW({
    mobile: false,
    //offset: 100,
});
wow.init();