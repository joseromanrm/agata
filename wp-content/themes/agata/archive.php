<?php
/**
 *
 * @package agata
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
?>

<div class="wrapper" id="archive-wrapper">

<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

	<div class="row">

		<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

		<main class="site-main" id="main">

			<?php if ( have_posts() ) : ?>

					<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
					?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php

					get_template_part( 'loop-templates/content', get_post_format() );
					?>

				<?php endwhile; ?>

			<?php else : ?>

				<?php get_template_part( 'loop-templates/content', 'none' ); ?>

			<?php endif; ?>

		</main>

		<?php nic_pagination(); ?>

	<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

</div> 

</div>

</div>

<?php get_footer(); ?>
