<?php
/**
 * @package agata
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container   = get_theme_mod( 'nic_container_type' );
?>

<div class="wrapper" id="author-wrapper">

<div class="container" id="content" tabindex="-1">

	<div class="row">

		<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

		<main class="site-main" id="main">

			<div class="page-header author-header">

				<?php
				$curauth = ( isset( $_GET['author_name'] ) ) ? get_user_by( 'slug',
					$author_name ) : get_userdata( intval( $author ) );
				?>

				<h1><?php esc_html_e( 'Acerca:', 'agata' ); ?><?php echo esc_html( $curauth->nickname ); ?></h1>

				<?php if ( ! empty( $curauth->ID ) ) : ?>
					<?php echo get_avatar( $curauth->ID ); ?>
				<?php endif; ?>

				<dl>
					<?php if ( ! empty( $curauth->user_url ) ) : ?>
						<dt><?php esc_html_e( 'Website', 'agata' ); ?></dt>
						<dd>
							<a href="<?php echo esc_url( $curauth->user_url ); ?>"><?php echo esc_html( $curauth->user_url ); ?></a>
						</dd>
					<?php endif; ?>

					<?php if ( ! empty( $curauth->user_description ) ) : ?>
						<dt><?php esc_html_e( 'Perfil', 'agata' ); ?></dt>
						<dd><?php echo esc_html( $curauth->user_description ); ?></dd>
					<?php endif; ?>
				</dl>

				<h2><?php esc_html_e( 'Posts by', 'agata' ); ?> <?php echo esc_html( $curauth->nickname ); ?>
					:</h2>

			</div>

			<ul>

				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<li>
							<a rel="bookmark" href="<?php the_permalink() ?>"
							   title="<?php esc_html_e( 'Permanent Link:', 'agata' ); ?> <?php the_title(); ?>">
								<?php the_title(); ?></a>,
							<?php nic_posted_on(); ?> <?php esc_html_e( 'in',
							'agata' ); ?> <?php the_category( '&' ); ?>
						</li>
					<?php endwhile; ?>

				<?php else : ?>

					<?php get_template_part( 'loop-templates/content', 'none' ); ?>

				<?php endif; ?>


			</ul>

		</main>

		<?php nic_pagination(); ?>

	<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

</div>
</div>
</div>

<?php get_footer(); ?>
