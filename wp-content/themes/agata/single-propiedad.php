<?php
/**
 * @package agata
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$province = trim(get_field('propiedad_ubicacion', $post_id)); 
$location = trim(get_field('propiedad_ubicacion_2', $post_id));
$price = trim(get_field('propiedad_precio', $post_id));
$images = get_field('propiedad_fotos', $post->ID );
$moneda = get_field('tipo_de_moneda', $post->ID );

if( empty($moneda) ){
	$currency   = get_theme_mod( 'nic_theme_currency' );
	if( empty(trim($currency)) ) $currency = "₡";
}else{
	$currency = $moneda;
}
?>

<div class="actions">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-12 align-self-center xs-center wow fadeInUp" data-wow-duration="100" data-wow-delay="0s">
				<a class="volver" href="<?php echo get_home_url(); ?>/propiedades/"><svg width="19px" height="33px" viewBox="0 0 19 33" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
 
    <defs>
        <polygon id="path-1" points="0 0.0003 18.9818 0.0003 18.9818 32.1323 0 32.1323"></polygon>
    </defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="v" transform="translate(-437.000000, -300.000000)">
            <g id="Page-1" transform="translate(437.000000, 300.000000)">
                <mask id="mask-2" fill="white">
                    <use xlink:href="#path-1"></use>
                </mask>
                <g id="Clip-2"></g>
                <path d="M15.4298,0.3296 L0.3268,15.2986 C-0.1092,15.7356 -0.1092,16.4756 0.3268,16.9116 L15.4298,31.8616 C15.8658,32.2226 16.5298,32.2226 16.9658,31.8616 L18.5788,30.2496 C19.0148,29.8126 19.0148,29.0736 18.5788,28.6366 L6.6828,16.8926 C6.2458,16.4556 6.2458,15.7166 6.6828,15.2796 L18.6548,3.5366 C19.0908,3.0996 19.0908,2.3596 18.6548,1.9236 L17.0418,0.3106 C16.6058,-0.1064 15.8848,-0.1064 15.4298,0.3296" id="Fill-1" fill="#000000" mask="url(#mask-2)"></path>
            </g>
        </g>
    </g>
</svg> Volver</a>
			</div>
			<div class="col-lg-6 col-md-6 col-12 xs-center xs-mt-30 right align-self-center wow fadeInUp" data-wow-duration="100" data-wow-delay="0.2s">
				<a target="blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" class="btn"><svg width="34px" height="34px" viewBox="0 0 34 34" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="v" transform="translate(-408.000000, -349.000000)" fill="#187F2E">
            <path d="M435.355,370.0428 C433.065,370.0428 431.083,371.2718 429.974,373.0918 L420.6,370.2808 C420.604,370.2008 420.625,370.1248 420.625,370.0428 C420.625,369.0238 420.362,368.0728 419.933,367.2228 L427.587,360.5248 C428.602,361.2178 429.825,361.6258 431.146,361.6258 C434.633,361.6258 437.459,358.7998 437.459,355.3138 C437.459,351.8268 434.633,349.0008 431.146,349.0008 C427.66,349.0008 424.834,351.8268 424.834,355.3138 C424.834,356.3318 425.096,357.2808 425.526,358.1328 L417.871,364.8308 C416.856,364.1388 415.633,363.7308 414.312,363.7308 C410.826,363.7308 408,366.5558 408,370.0428 C408,373.5298 410.826,376.3558 414.312,376.3558 C416.599,376.3558 418.585,375.1268 419.693,373.3068 L429.067,376.1178 C429.063,376.1998 429.042,376.2728 429.042,376.3558 C429.042,379.8418 431.868,382.6678 435.355,382.6678 C438.841,382.6678 441.667,379.8418 441.667,376.3558 C441.667,372.8688 438.841,370.0428 435.355,370.0428" id="Page-1"></path>
        </g>
    </g>
</svg> Compartir</a>
			</div>
		</div>
	</div>
</div>

<div class="description">
	<div class="container">
		<div class="row">
			<?php
				
				if( !empty($province) ){
			?>
			<div class="col-lg-12 mb-50 wow fadeInUp" data-wow-duration="100" data-wow-delay="0s">
					<?php
						if( !empty($location) )
							echo '<h4>Ubicación: '.$location.', '.$province.'</h4>';
						else
							echo '<h4>Ubicación: '.$province.'</h4>'; 
					?>
			</div>
			<?php }?>
			
			<?php  if(!empty($images)){ ?>
			<div class="col-lg-8 col-md-6 xs-mb-30 wow fadeInUp" data-wow-duration="100" data-wow-delay="0s">
				<div class="gallery">
					<div class="price"><?php echo $currency; ?><?php echo number_format($price); ?></div>

					<a href="#" class="carousel-left"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/carousel-left.svg" alt=""></a>
					<a href="#" class="carousel-right"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/carousel-right.svg" alt=""></a>

					<div id="sync1" class="owl-carousel owl-theme">
						<?php foreach ($images as $image) { //var_dump($image); ?>
						<div class="item">
							<img src="<?php echo $image['url']; ?>" alt="">
						</div>
						<?php } ?>
					</div>
				</div>

				<div id="sync2" class="owl-carousel owl-theme">
						<?php foreach ($images as $image) { ?>
						<div class="item">
							<img src="<?php echo $image['sizes']['medium']; ?>" alt="">
						</div>
						<?php } ?>
				</div>
			</div>
			<?php }?>

			<div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-duration="100" data-wow-delay="0.2s">
				<?php
					$description = get_field('propiedad_descripcion', $post->ID );
					if( !empty($description) ){
				?>
				<p>DESCRIPCIÓN:</p>
				
				<?php 
						echo $description;
					}
				?>

				<div class="info-description">
					<h5>Contáctenos</h5>
					<?php 
						$my_email = get_option( 'admin_email' );
						echo '<a class="mail" href="mailto:'.$my_email.'">'.$my_email.'</a>';
					?>
					
					<?php get_template_part( 'global-templates/links-hf' ); ?>
				</div>

			</div>
		</div>
	</div>
</div>


<?php
	// Obtener las características chequeadas en la propiedad.
	$terms = get_the_terms( $post->ID , 'caracteristica' );
	if( !empty($terms) ){
?>
<div class="features">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3>Características</h3>
			</div>

			<?php
				if ( $terms != null ){
					$time = 0;
					foreach( $terms as $caracteristica ) {
						//print $term->term_id;
						$caracteristica_imagen = get_field('icono_caracteristica', $caracteristica->taxonomy . '_' . $caracteristica->term_id);   
						if (!empty($caracteristica_imagen)){
							echo '<div class="col-lg-4 col-md-6 col-12 col-border mb-0 wow fadeInUp" data-wow-duration="100" data-wow-delay="'.$time.'s">';
							echo '<ul><li><img width="33" height="21" src="'.$caracteristica_imagen.'" alt=""> '.$caracteristica->name.'</li></ul>';
							echo '</div>';
						}
						else{
							echo '<div class="col-lg-4 col-md-6 col-12 col-border mb-0 wow fadeInUp" data-wow-duration="100" data-wow-delay="'.$time.'s">';
							echo '<ul><li class="none">'.$caracteristica->name.'</li></ul>';
							echo '</div>';
						}
						$time = $time + 0.1;
						unset($caracteristica); 
					}
				}

			?>
		</div>
	</div>
</div>
<?php } ?>

<?php get_template_part( 'global-templates/similar' ); ?>
<?php get_footer(); ?>
