<?php
/**
 *
 * @link https://jetpack.me/
 *
 * @package agata
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

add_action( 'after_setup_theme', 'nic_components_jetpack_setup' );

if ( ! function_exists ( 'nic_components_jetpack_setup' ) ) {
	function nic_components_jetpack_setup() {
		add_theme_support( 'infinite-scroll', array(
			'container' => 'main',
			'render'    => 'nic_components_infinite_scroll_render',
			'footer'    => 'page',
		) );

		add_theme_support( 'jetpack-responsive-videos' );

		add_theme_support( 'jetpack-social-menu' );

	}
}

if ( ! function_exists ( 'nic_components_infinite_scroll_render' ) ) {
	function nic_components_infinite_scroll_render() {
		while ( have_posts() ) {
			the_post();
			if ( is_search() ) :
				get_template_part( 'loop-templates/content', 'search' );
			else :
				get_template_part( 'loop-templates/content', get_post_format() );
			endif;
		}
	}
}

if ( ! function_exists ( 'nic_components_social_menu' ) ) {
	function nic_components_social_menu() {
		if ( ! function_exists( 'jetpack_social_menu' ) ) {
			return;
		} else {
			jetpack_social_menu();
		}
	}
}