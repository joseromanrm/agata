<?php
/**
 * Check and setup theme's default settings
 *
 * @package agata
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists ( 'agata_setup_theme_default_settings' ) ) {
	function agata_setup_theme_default_settings() {

		$nic_posts_index_style = get_theme_mod( 'nic_posts_index_style' );
		if ( '' == $nic_posts_index_style ) {
			set_theme_mod( 'nic_posts_index_style', 'default' );
		}

		$nic_sidebar_position = get_theme_mod( 'nic_sidebar_position' );
		if ( '' == $nic_sidebar_position ) {
			set_theme_mod( 'nic_sidebar_position', 'right' );
		}

		$nic_container_type = get_theme_mod( 'nic_container_type' );
		if ( '' == $nic_container_type ) {
			set_theme_mod( 'nic_container_type', 'container' );
		}
	}
}