<?php
/**
 * @package agata
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


add_action( 'after_setup_theme', 'nic_wpcom_setup' );

if ( ! function_exists ( 'nic_wpcom_setup' ) ) {
	function nic_wpcom_setup() {
		global $themecolors;

		if ( ! isset( $themecolors ) ) {
			$themecolors = array(
				'bg'     => '',
				'border' => '',
				'text'   => '',
				'link'   => '',
				'url'    => '',
			);
		}
		
		add_theme_support( 'print-styles' );
	}
}


add_action( 'wp_enqueue_scripts', 'nic_wpcom_styles' );

if ( ! function_exists ( 'nic_wpcom_styles' ) ) {
	function nic_wpcom_styles() {
		wp_enqueue_style( 'understrap-wpcom', get_template_directory_uri() . '/inc/style-wpcom.css', '20160411' );
	}
}