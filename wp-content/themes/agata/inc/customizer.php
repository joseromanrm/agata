<?php
/**
 *
 * @package agata
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'nic_customize_register' ) ) {
	function nic_customize_register( $wp_customize ) {
		$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
		$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
		$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	}
}
add_action( 'customize_register', 'nic_customize_register' );

if ( ! function_exists( 'nic_theme_customize_register' ) ) {

	function nic_theme_customize_register( $wp_customize ) {

		// Theme layout settings.
		$wp_customize->add_section( 'nic_theme_layout_options', array(
			'title'       => __( 'Configuración', 'agata' ),
			'capability'  => 'edit_theme_options',
			'description' => __( 'Container width and sidebar defaults', 'agata' ),
			'priority'    => 160,
		) );


        	function nic_theme_slug_sanitize_select( $input, $setting ){

            		$input = sanitize_key( $input );

           		$choices = $setting->manager->get_control( $setting->id )->choices;

            		return ( array_key_exists( $input, $choices ) ? $input : $setting->default );                

			}
			
			$wp_customize->add_setting( 'nic_theme_copyright', array(
				'default'           => '© 2018 Agata Bienes Raíces',
				'type'              => 'theme_mod',
				'sanitize_callback' => 'sanitize_text_field',
				'capability'        => 'edit_theme_options',
			) );
	
			$wp_customize->add_control(
				new WP_Customize_Control(
					$wp_customize,
					'nic_theme_copyright', array(
						'label'       => __( 'Copyright', 'agata' ),
						'description' => __( '',
						'agata' ),
						'section'     => 'nic_theme_layout_options',
						'settings'    => 'nic_theme_copyright',
						'type'        => 'text',
						'priority'    => '20',
					)
				) );


				$wp_customize->add_setting( 'nic_theme_currency', array(
					'default'           => '',
					'type'              => 'theme_mod',
					'sanitize_callback' => 'sanitize_text_field',
					'capability'        => 'edit_theme_options',
				) );
			
				$wp_customize->add_control(
						new WP_Customize_Control(
							$wp_customize,
							'nic_theme_currency', array(
								'label'       => __( 'Moneda', 'agata' ),
								'description' => __( 'Moneda a visualizar: $ / ₡', 'agata' ),
								'section'     => 'nic_theme_layout_options',
								'settings'    => 'nic_theme_currency',
								'type'        => 'text',
								'priority'    => '20',
							)
				) );

			$wp_customize->add_setting( 'nic_theme_phone', array(
					'default'           => '2229-1111',
					'type'              => 'theme_mod',
					'sanitize_callback' => 'sanitize_text_field',
					'capability'        => 'edit_theme_options',
			) );
		
			$wp_customize->add_control(
					new WP_Customize_Control(
						$wp_customize,
						'nic_theme_phone', array(
							'label'       => __( 'Teléfono', 'agata' ),
							'description' => __( '',
							'agata' ),
							'section'     => 'nic_theme_layout_options',
							'settings'    => 'nic_theme_phone',
							'type'        => 'text',
							'priority'    => '20',
						)
			) );

			$wp_customize->add_setting( 'nic_theme_code', array(
				'default'           => '',
				'type'              => 'theme_mod',
				'sanitize_callback' => 'sanitize_text_field',
				'capability'        => 'edit_theme_options',
			) );
		
			$wp_customize->add_control(
					new WP_Customize_Control(
						$wp_customize,
						'nic_theme_code', array(
							'label'       => __( 'Código', 'agata' ),
							'description' => __( 'Cod. de País + Cod. de área',
							'agata' ),
							'section'     => 'nic_theme_layout_options',
							'settings'    => 'nic_theme_code',
							'type'        => 'text',
							'priority'    => '20',
						)
			) );

			$wp_customize->add_setting( 'nic_theme_whatsapp', array(
				'default'           => '7074-0424',
				'type'              => 'theme_mod',
				'sanitize_callback' => 'sanitize_text_field',
				'capability'        => 'edit_theme_options',
			) );
		
			$wp_customize->add_control(
					new WP_Customize_Control(
						$wp_customize,
						'nic_theme_whatsapp', array(
							'label'       => __( 'Whatsapp', 'agata' ),
							'description' => __( '',
							'agata' ),
							'section'     => 'nic_theme_layout_options',
							'settings'    => 'nic_theme_whatsapp',
							'type'        => 'text',
							'priority'    => '20',
						)
			) );

			$wp_customize->add_setting( 'nic_theme_fb_link', array(
				'default'           => 'https://facebook.com',
				'type'              => 'theme_mod',
				'sanitize_callback' => 'sanitize_text_field',
				'capability'        => 'edit_theme_options',
			) );
		
			$wp_customize->add_control(
					new WP_Customize_Control(
						$wp_customize,
						'nic_theme_fb_link', array(
							'label'       => __( 'Página de Facebook (URL)', 'agata' ),
							'description' => __( '',
							'agata' ),
							'section'     => 'nic_theme_layout_options',
							'settings'    => 'nic_theme_fb_link',
							'type'        => 'text',
							'priority'    => '20',
						)
			) );

		$wp_customize->add_setting( 'nic_container_type', array(
			'default'           => 'container',
			'type'              => 'theme_mod',
			'sanitize_callback' => 'nic_theme_slug_sanitize_select',
			'capability'        => 'edit_theme_options',
		) );
		

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'nic_container_type', array(
					'label'       => __( 'Container Width', 'agata' ),
					'description' => __( '', 'agata' ),
					'section'     => 'nic_theme_layout_options',
					'settings'    => 'nic_container_type',
					'type'        => 'select',
					'choices'     => array(
						'container'       => __( 'Fixed width container', 'agata' ),
						'container-fluid' => __( 'Full width container', 'agata' ),
					),
					'priority'    => '10',
				)
			) );

		$wp_customize->add_setting( 'nic_sidebar_position', array(
			'default'           => 'right',
			'type'              => 'theme_mod',
			'sanitize_callback' => 'sanitize_text_field',
			'capability'        => 'edit_theme_options',
		) );

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'nic_sidebar_position', array(
					'label'       => __( 'Sidebar Positioning', 'agata' ),
					'description' => __( '','agata' ),
					'section'     => 'nic_theme_layout_options',
					'settings'    => 'nic_sidebar_position',
					'type'        => 'select',
					'sanitize_callback' => 'nic_theme_slug_sanitize_select',
					'choices'     => array(
						'right' => __( 'Right sidebar', 'agata' ),
						'left'  => __( 'Left sidebar', 'agata' ),
						'both'  => __( 'Left & Right sidebars', 'agata' ),
						'none'  => __( 'No sidebar', 'agata' ),
					),
					'priority'    => '20',
				)
			) );
	}
} 
add_action( 'customize_register', 'nic_theme_customize_register' );


if ( ! function_exists( 'nic_customize_preview_js' ) ) {

	function nic_customize_preview_js() {
		wp_enqueue_script( 'nic_customizer', get_template_directory_uri() . '/js/customizer.js',
			array( 'customize-preview' ), '20130508', true
		);
	}
}
add_action( 'customize_preview_init', 'nic_customize_preview_js' );
