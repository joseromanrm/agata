<?php
/**
 * @package agata
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! isset( $content_width ) ) {
	$content_width = 640; 
}

add_action( 'after_setup_theme', 'nic_setup' );

if ( ! function_exists ( 'nic_setup' ) ) {

	function nic_setup() {
		load_theme_textdomain( 'agata', get_template_directory() . '/languages' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );

		register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'agata' ),
		) );

		register_nav_menus( array(
			'footer' => __( 'Footer', 'agata' ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_theme_support( 'post-thumbnails' );
		add_image_size( 'property-thumbnail-size', 349, 325, true );
		add_image_size( 'property-gallery-size', 665, 498, true );
		add_image_size( 'header-image-size', 1920, 524, true );

		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support( 'post-formats', array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
		) );

		add_theme_support( 'custom-background', apply_filters( 'nic_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		add_theme_support( 'custom-logo' );
		agata_setup_theme_default_settings();

	}
}

add_filter( 'excerpt_more', 'nic_custom_excerpt_more' );
if ( ! function_exists( 'nic_custom_excerpt_more' ) ) {
	function nic_custom_excerpt_more( $more ) {
		return '';
	}
}

add_filter( 'wp_trim_excerpt', 'nic_all_excerpts_get_more_link' );

if ( ! function_exists( 'nic_all_excerpts_get_more_link' ) ) {
	function nic_all_excerpts_get_more_link( $post_excerpt ) {
		return $post_excerpt . ' [...]<p><a class="btn btn-secondary" href="' . esc_url( get_permalink( get_the_ID() )) . '">' . __( 'Leer más...',
		'agata' ) . '</a></p>';
	}
}