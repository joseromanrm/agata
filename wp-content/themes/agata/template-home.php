<?php /* Template Name: Home */ ?>

<?php get_header(); 
	
?>

<div class="recents">
	<div class="container">

		<div class="row">
			<div class="col-lg-12">
				<h3 class="wow fadeInUp" data-wow-duration="100" data-wow-delay="0">Propiedades recientes</h3>
			</div>
			<?php
				$query = new WP_Query(array(
					'post_type' => 'propiedad',
					'posts_per_page' => 6,
                    'post_status' => 'publish',
                    'orderby' => 'random'
				));
				
				while ($query->have_posts()) {
					$query->the_post();
					$post_id = get_the_ID();

					$province = trim(get_field('propiedad_ubicacion', $post_id));
					$location = trim(get_field('propiedad_ubicacion_2', $post_id));
					$price = trim(get_field('propiedad_precio', $post_id));
					$url = get_permalink();

					$thumb_id = get_post_thumbnail_id();
                    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'property-thumbnail-size', true);
					$imagen = $thumb_url_array[0];

					$moneda = get_field('tipo_de_moneda', $post_id );
					if( empty($moneda) ){
						$currency   = get_theme_mod( 'nic_theme_currency' );
						if( empty(trim($currency)) ) $currency = "₡";
					}else{
						$currency = $moneda;
					}
									
			?>
			<div class="col-lg-4 col-md-6 col-12 wow fadeInUp" data-wow-duration="100" data-wow-delay="0s">
				<div class="properties" data-url="<?php echo $url; ?>">
					<div class="property">
						<div class="hover"><a href="#">Info</a></div>
						<div class="location"><?php echo $location . ', ' . $province; ?></div>
						<?php 
							if(!empty($price)){
								echo '<div class="price">'.$currency.number_format($price).'</div>';
							}
						?>
						<img class="img-fluid" src="<?php echo $imagen; ?>" alt="">
					</div>
					<div class="title"><?php echo get_the_title(); ?></div>
				</div>
			</div>
			<?php
				}
				
				wp_reset_query();
			?>

			<div class="col-lg-12 wow fadeInUp" data-wow-duration="100" data-wow-delay="0">
				<a href="<?php echo get_home_url(); ?>/propiedades/" class="btn btn-lg btn-block see-all-properties">Ver todas las propiedades <img src="<?php echo get_template_directory_uri(); ?>/img/button-arrow-right.svg" alt=""></a>
			</div>
		
		</div>
	</div>
</div>

<?php
	$featured_property = new WP_Query(array(
		'post_type' => 'propiedad',
		'posts_per_page' => -1,
		'post_status' => 'publish',
	));

	while ($featured_property->have_posts()) {
		$featured_property->the_post();
		$post_id = get_the_ID();

		$is_featured_property = trim(get_field('propiedad_destacada', $post_id));
		if ($is_featured_property == true){

			$featured_images = get_field('propiedad_fotos', $post->ID );
	
?>
<div class="featured-property">
	<div class="container">
		<div class="row">
				<div class="col-lg-6">
					<h2 class="wow fadeInUp" data-wow-duration="100" data-wow-delay="0s">Excelente <br>
						<strong>oportunidad</strong> <br> en 
						<?php echo get_the_title(); ?></h2>

					<?php
						// Obtener las características chequeadas en la propiedad.
						$terms = get_the_terms( $post->ID , 'caracteristica' );
						if( !empty($terms) ){
					?>
							<div class="row">
								<?php
									if ( $terms != null ){
										$time = 0;
										foreach( $terms as $caracteristica ) {
											//print $term->term_id;
											$caracteristica_imagen = get_field('icono_caracteristica', $caracteristica->taxonomy . '_' . $caracteristica->term_id);   
											if (!empty($caracteristica_imagen)){
												echo '<div class="col-lg-12 col-md-6 col-12 col-border mb-0 wow fadeInUp" data-wow-duration="100" data-wow-delay="'.$time.'s">';
												echo '<ul><li><img width="33" height="21" src="'.$caracteristica_imagen.'" alt=""> '.$caracteristica->name.'</li></ul>';
												echo '</div>';
											}
											else{
												echo '<div class="col-lg-12 col-md-6 col-12 col-border mb-0 wow fadeInUp" data-wow-duration="100" data-wow-delay="'.$time.'s">';
												echo '<ul><li class="none">'.$caracteristica->name.'</li></ul>';
												echo '</div>';
											}
											$time = $time + 0.1;
											unset($caracteristica); 
										}
									}

								?>
							</div>

					<?php } ?>
				</div>

				<div class="col-lg-6">
					<?php  if(!empty($featured_images)){ ?>
						<div class="container-gallery">
							<div class="gallery">
								<div class="price"><?php echo $currency; ?><?php echo number_format($price); ?></div>

								<a href="#" class="carousel-left"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/carousel-left.svg" alt=""></a>
								<a href="#" class="carousel-right"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/carousel-right.svg" alt=""></a>

								<div id="sync1" class="owl-carousel owl-theme">
									<?php foreach ($featured_images as $f_image) { //var_dump($f_image); ?>
									<div class="item">
										<img src="<?php echo $f_image['url']; ?>" alt="">
									</div>
									<?php } ?>
								</div>
							</div>

							<div id="sync2" class="owl-carousel owl-theme">
									<?php foreach ($featured_images as $f_image) { ?>
										<div class="item">
											<img src="<?php echo $f_image['sizes']['medium']; ?>" alt="">
										</div>
									<?php } ?>
							</div>
						</div>
					<?php }?>

					<a href="<?php echo get_permalink(); ?>" class="btn btn-lg btn-block more-info">Más información <img src="<?php echo get_template_directory_uri(); ?>/img/button-arrow-right.svg" alt=""></a>
					<a href="<?php echo get_home_url(); ?>/propiedades/" class="btn btn-lg btn-block see-all-properties">Ver todas las propiedades <img src="<?php echo get_template_directory_uri(); ?>/img/button-arrow-right.svg" alt=""></a>
				</div>
		</div>
	</div>
</div>
<?php 
			break;
		}
	} 
?>



<?php get_footer(); ?>

