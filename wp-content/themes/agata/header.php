<?php
/**
 * The header for our theme.
 *
 * @package agata
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'nic_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900" rel="stylesheet">

	

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div class="loader">
		<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
	</div>

	<a href="#" id="back-to-top" title="Ir arriba"><svg width="60px" height="35px" viewBox="0 0 60 35" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <defs>
        <polygon id="path-1" points="0 0 59.2895979 0 59.2895979 35.025 0 35.025"></polygon>
    </defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="v" transform="translate(-429.000000, -240.000000)">
            <g id="Page-1" transform="translate(429.000000, 240.000000)">
                <mask id="mask-2" fill="white">
                    <use xlink:href="#path-1"></use>
                </mask>
                <g id="Clip-2"></g>
                <path d="M58.6814,28.4703 L31.0604,0.6043 C30.2554,-0.2017 28.8904,-0.2017 28.0844,0.6043 L0.4984,28.4703 C-0.1666,29.2753 -0.1666,30.5003 0.4984,31.3053 L3.4744,34.2813 C4.2804,35.0863 5.6454,35.0863 6.4504,34.2813 L28.1194,12.3313 C28.9244,11.5263 30.2894,11.5263 31.0954,12.3313 L52.7654,34.4213 C53.5704,35.2263 54.9354,35.2263 55.7414,34.4213 L58.7164,31.4463 C59.4864,30.6403 59.4864,29.3103 58.6814,28.4703" id="Fill-1" fill="#000000" mask="url(#mask-2)"></path>
            </g>
        </g>
    </g>
</svg></a>

	<header>
	
		<div class="top">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 right">
						<div class="logo">
							<a href="<?php echo get_home_url(); ?>">
								<?php
									// Obtener el logo del sitio
									$custom_logo_id = get_theme_mod( 'custom_logo' );
									if( !empty($custom_logo_id) ) { // si se ha agregado un logo al customizer.
										$logo_image_temp = wp_get_attachment_image_src( $custom_logo_id , 'full' );
										$logo_image = $logo_image_temp[0];
									}else{
										$logo_image = get_template_directory_uri() . '/img/logo.svg';
									}
								?>
								<img src="<?php echo $logo_image; ?>" alt="">
							</a>
						</div>

						<?php get_template_part( 'global-templates/links-hf' ); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="menu apply-sticky">
			<div class="container">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-4 align-self-center">
						<a class="logo-icon" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/icon.svg" alt=""></a>
					</div>
					<div class="col-lg-10 col-md-10 col-8 right">
						<div class="menu-responsive">
								<button class="hamburger hamburger--spin" type="button">
									<span class="hamburger-box">
										<span class="hamburger-inner"></span>
									</span>
								</button>  
						</div>
						<div class="top">
							<?php 
								wp_nav_menu( array( 'theme_location' => 'primary' ) ); 
							?>
						</div>
					</div>
				</div>
			</div>

			<div class="menu-mobile">
				<?php 
					wp_nav_menu( array( 'theme_location' => 'primary' ) ); 
				?>
			</div>
		</div>

		<div class="container container-title">
			<div class="row bloque">
				<?php
					if( is_front_page() )
						$titleClass = 'col-lg-8 col-md-6';
					else
						$titleClass = 'col-lg-9 offset-lg-3 col-md-8 offset-md-4 right';
				?>
				<div class="<?php echo $titleClass; ?> align-self-end">
					<h1 class="<?php if( !is_front_page() ) echo 'right'; ?>"><?php 


						if ( is_tax() ) {
							$term_page = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
							echo  $term_page->name;
				 
						}else{
							// Mostrar el Nombre de la página
							$pageName = trim(get_field('titulo_pagina')); 
							if( !empty($pageName) ) 
								echo $pageName; 
							else 
								echo get_the_title();
						}

					?></h1>
				</div>

				<div class="col-lg-4 col-md-6 container-search">
				<div class="buscador">
								<h3><img src="<?php echo get_template_directory_uri(); ?>/img/home/search.svg" alt=""> <span>Buscador</span></h3>
								<form method="post" id="searchform" action="<?php echo get_home_url(); ?>/propiedades/" role="search">
									<div class="row">
										<div class="col-lg-5 col-md-5 col-5 align-self-center pr-0">
											Tipo de Propiedad
										</div>
										<div class="col-lg-7 col-md-7 col-7">
											<?php
												$args = array (
													'type' => 'propiedad', //your custom post type
													'orderby' => 'name',
													'order' => 'ASC',
													'taxonomy' => 'tipo',
													'hide_empty' => 0 //shows empty categories
												);
												$categories = get_categories( $args );
												if( !empty($categories) ){
											?>
											<select name="tipo" id="tipo">
												<option value="">Todos</option>
													<?php 
														foreach ($categories as $category) {   
															echo '<option value="'.$category->slug.'">'.$category->name.'</option>'; 
														}
													?>
											</select>
											<?php } ?>
										</div>
									</div>

									<div class="row">
										<div class="col-lg-5 col-md-5 col-5 align-self-center pr-0">
											Ubicación
										</div>
										<div class="col-lg-7 col-md-7 col-7">
										<div class="box-select">
											<select name="lugar" id="lugar">
												<option value="">Todos</option>
												<option value="san jose">San José</option>
												<option value="alajuela">Alajuela</option>
												<option value="cartago">Cartago</option>
												<option value="heredia">Heredia</option>
												<option value="guanacaste">Guanacaste</option>
												<option value="puntarenas">Puntarenas</option>
												<option value="limón">Limón</option>
											</select>
											</div>
										</div>
									</div>

									<!--div class="row">
										<div class="col-lg-5 col-md-5 col-5 align-self-center pr-0">
											Rango de Precio
										</div>
										<div class="col-lg-7 col-md-7 col-7">
											<div class="box-select">
											<select name="rango_precio" id="rango_precio">
												<option value="all">Todos</option>
												<option value="1-50000">$0 - $50,000</option>
												<option value="50001-100000">$50,000 - $100,000</option>
												<option value="100001-200000">$100,000 - $200,000</option>
												<option value="200001-300000">$200,000 - $300,000</option>
												<option value="300001-400000">$300,000 - $400,000</option>
												<option value="400001-500000">$400,000 - $500,000</option>
											</select>
											</div>
										</div>
									</div-->

									<div class="row">
										<div class="col-lg-12">
											<button type="submit">Buscar</button>
										</div>
									</div>

								</form>
							</div>
				</div>
			</div>
		</div>

		
		<?php
			$imagen = get_template_directory_uri() . '/img/home/banner.jpg';

			// si se introdujo una imagen personalizada:
			$imagen_cabecera = trim(get_field('imagen_cabecera')); 
			if( !empty($imagen_cabecera) ) 
			{
				//$imagen = $imagen_cabecera;
				$image_array = wp_get_attachment_image_src($imagen_cabecera, 'header-image-size'); // 1920x524
				$imagen = $image_array[0];
			}
			


		?>
		<div class="bg" style="background: url('<?php echo $imagen; ?>') no-repeat top center black; background-size: cover;"></div>

		
	</header>

	<?php get_template_part('global-templates/tipos'); ?>