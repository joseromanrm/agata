<?php
/**
 *
 * @package agata
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container   = get_theme_mod( 'nic_container_type' );
$sidebar_pos = get_theme_mod( 'nic_sidebar_position' );

?>
<section class="no-results not-found">

<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<img src="<?php echo get_template_directory_uri(); ?>/img/search.svg" alt="">
			<h2 class="page-title">
				<?php esc_html_e( 'Página no encontrada.', 'agata' ); ?>
			</h2>
		</div>
	</div>
</div>

</section>

<?php get_footer(); ?>
